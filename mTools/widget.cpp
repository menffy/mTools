#include "widget.h"
#include "ui_widget.h"

#include <QLabel>
#include <QFontDatabase>
#include <QDebug>
#include <QFrame>
#include <QMessageBox>

#include "mnavmenu.h"
#include "menuItem.h"
#include "desktop.h"
#include "test.h"
#include "test1.h"
#include "mtabmenu.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    initFont();
    init();
    initMenu();
    initDesktop();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::openNewWidget(menuItem menuInfo)
{
    bool isExists=false;

    for(int i=0;i<ui->rightFrame->count();i++)
    {
        QWidget *widget= ui->rightFrame->widget(i);

        if(widget->objectName()==QString("page_%1").arg(menuInfo.id))
        {
            ui->rightFrame->setCurrentWidget(widget);
            isExists=true;
            break;
        }
    }

    if(!isExists)
    {
        if(menuInfo.widgetName=="test")
        {
            test *t=new test(this);
            t->setObjectName(QString("page_%1").arg(menuInfo.id));
            ui->rightFrame->addWidget(t);
            ui->rightFrame->setCurrentWidget(t);
        }
        else if(menuInfo.widgetName=="test1")
        {
            test1 *t1=new test1(this);
            t1->setObjectName(QString("page_%1").arg(menuInfo.id));
            ui->rightFrame->addWidget(t1);
            ui->rightFrame->setCurrentWidget(t1);
        }
        else if(menuInfo.widgetName=="desktop")
        {
            DeskTop *desktop=new DeskTop(this);
            desktop->setObjectName(QString("page_%1").arg(menuInfo.id));
            ui->rightFrame->addWidget(desktop);
            ui->rightFrame->setCurrentWidget(desktop);
        }
    }

    setTab(menuInfo);
}

void Widget::closeTab(menuItem menuInfo)
{
    QList<mTabMenu *> tabmenus=ui->tabFrame->findChildren<mTabMenu *>();

    for(int i=0;i<tabmenus.count();i++)
    {
        mTabMenu *menu=tabmenus.at(i);

        if(menu->property("tabId")==QString("%1").arg(menuInfo.id))
        {
            delete menu;
            removeNewWidget(menuInfo);
            break;
        }
    }

    initDesktop();
}

void Widget::selectTabClick(menuItem menuInfo)
{
    openNewWidget(menuInfo);
}

void Widget::fullScreen(bool flag)
{
    if(flag)
    {
        this->showFullScreen();
    }
    else
    {
        this->showNormal();
    }
}

void Widget::initFont()
{
    int fontId = QFontDatabase::addApplicationFont(":/fonts/fontawesome-webfont.ttf");
    QStringList fontFamilies = QFontDatabase::applicationFontFamilies(fontId);

    mFont.setFamily(fontFamilies.at(0));
    mFont.setPointSize(10);
}

void Widget::initMenu()
{
    QList<menuItem> menuItems;

    menuItem item1;

    item1.id=1;
    item1.parentId=0;
    item1.title="敏捷开发";
    item1.icon=0xf014;
    item1.level=0;
    item1.isSub=true;

    menuItems.append(item1);

    menuItem item11;

    item11.id=2;
    item11.widgetName="test";
    item11.parentId=1;
    item11.title="代码生成";
    item11.icon=0xf017;
    item11.level=1;
    item11.isSub=false;

    menuItems.append(item11);

    menuItem item2;

    item2.id=3;
    item2.parentId=0;
    item2.title="系统管理";
    item2.icon=0xf011;
    item2.level=0;
    item2.isSub=true;

    menuItems.append(item2);

    menuItem item22;

    item22.id=5;
    item22.widgetName="test1";
    item22.parentId=3;
    item22.title="用户管理";
    item22.icon=0xf018;
    item22.level=1;
    item22.isSub=false;

    menuItems.append(item22);

    menuItem item3;

    item3.id=4;
    item3.parentId=0;
    item3.title="组织机构";
    item3.icon=0xf016;
    item3.level=0;
    item3.isSub=true;

    menuItems.append(item3);

    mNavMenu *menu=new mNavMenu(mFont,ui->leftFrame);
    menu->setMenus(menuItems);

    connect(menu,SIGNAL(openNewWidget(menuItem)),this,SLOT(openNewWidget(menuItem)));
}

void Widget::initDesktop()
{
    menuItem desktop;
    desktop.id=0;
    desktop.widgetName="desktop";
    desktop.title="我的桌面";

    openNewWidget(desktop);
}

void Widget::init()
{
    setSkin("");

    tab_horizontalSpacer_right = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    ui->btnFullScreen->setCursor(Qt::PointingHandCursor);
    ui->btnFullScreen->setFont(mFont);
    ui->btnFullScreen->setText(QChar(0xf0b2));
    ui->btnFullScreen->setCheckable(true);

    connect(ui->btnFullScreen,SIGNAL(clicked(bool)),this,SLOT(fullScreen(bool)));
}

void Widget::setSkin(QString skinName="")
{
    QString css="";

    if (skinName.isEmpty()) {

        QFile file(":/css/default.css");
        file.open(QFile::ReadOnly);

        css=QString(file.readAll());

        file.close();
    }

    this->setStyleSheet(css);

}

void Widget::setTab(menuItem menuInfo)
{
    bool isExsits=false;

    QList<mTabMenu *> tabmenus=ui->tabFrame->findChildren<mTabMenu *>();

    for(int i=0;i<tabmenus.count();i++)
    {
        mTabMenu *menu=tabmenus.at(i);

        if(menu->property("tabId")==QString("%1").arg(menuInfo.id))
        {
            menu->setStyleSheet("background-color:#444;");
            menu->show();
            isExsits=true;
        }
        else
        {
            menu->setStyleSheet("background-color:none;");
        }
    }

    if(!isExsits)
    {
        mTabMenu *tabMenu=new mTabMenu(menuInfo,mFont,ui->tabFrame);
        tabMenu->setProperty("tabFrame","tabmenu");
        tabMenu->setProperty("tabId",QString("%1").arg(menuInfo.id));
        tabMenu->setCursor(Qt::PointingHandCursor);

        connect(tabMenu,SIGNAL(closeTabClick(menuItem)),this,SLOT(closeTab(menuItem)));
        connect(tabMenu,SIGNAL(selectTabClick(menuItem)),SLOT(selectTabClick(menuItem)));

        ui->horizontalLayout->addWidget(tabMenu);

        ui->horizontalLayout->removeItem(tab_horizontalSpacer_right);
        ui->horizontalLayout->addItem(tab_horizontalSpacer_right);
    }
}

void Widget::removeNewWidget(menuItem menuInfo)
{
    for(int i=0;i<ui->rightFrame->count();i++)
    {
        QWidget *widget= ui->rightFrame->widget(i);

        if(widget->objectName()==QString("page_%1").arg(menuInfo.id))
        {
            delete widget;
            break;
        }
    }
}

