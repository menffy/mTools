#ifndef DESKTOP_H
#define DESKTOP_H

#include <QWidget>

namespace Ui {
class DeskTop;
}

class DeskTop : public QWidget
{
    Q_OBJECT

public:
    explicit DeskTop(QWidget *parent = nullptr);
    ~DeskTop();

private:
    Ui::DeskTop *ui;
};

#endif // DESKTOP_H
