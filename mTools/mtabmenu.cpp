#include "mtabmenu.h"

#include <QGridLayout>
#include <QLabel>
#include <QStyleOption>
#include <QPainter>

mTabMenu::mTabMenu(menuItem menuInfo,QFont font,QWidget *parent) : QWidget(parent)
{
    this->setMouseTracking(true);

    mMenu=menuInfo;
    mFont=font;

    init();
}

void mTabMenu::closeTabBtn()
{
    emit closeTabClick(mMenu);
}

void mTabMenu::titleTabBtn()
{
    emit selectTabClick(mMenu);
}

mTabMenu::init()
{
    gridLayout = new QGridLayout(this);
    gridLayout->setMargin(0);
    gridLayout->setSpacing(0);

    horizontalSpacer_left = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    gridLayout->addItem(horizontalSpacer_left, 0, 0, 1, 1);

    horizontalSpacer_right = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    gridLayout->addItem(horizontalSpacer_right, 0, 3, 1, 1);

    titleBtn = new QPushButton(this);
    titleBtn->setFont(mFont);
    titleBtn->setText(mMenu.title);

    connect(titleBtn,SIGNAL(clicked()),this,SLOT(titleTabBtn()));

    gridLayout->addWidget(titleBtn, 0, 1, 1, 1);

    if(mMenu.id>0)
    {
        closeBtn = new QPushButton(this);
        closeBtn->setProperty("tabAttr","tabclose");
        closeBtn->setFont(mFont);
        closeBtn->setText(QChar(0xf00d));

        connect(closeBtn,SIGNAL(clicked()),this,SLOT(closeTabBtn()));

        gridLayout->addWidget(closeBtn, 0, 2, 1, 1);
    }
}

void mTabMenu::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.init(this);

    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void mTabMenu::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
        emit selectTabClick(mMenu);
    }
}
