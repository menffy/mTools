#include "msplashscreen.h"
#include "ui_msplashscreen.h"

#include <QVariant>
#include <QRect>

mSplashScreen::mSplashScreen(const QPixmap &pixmap, QWidget *parent):
    QSplashScreen(parent,pixmap),
    ui(new Ui::mSplashScreen)
{
    ui->setupUi(this);
    bannerMap=pixmap;
    init();
}

mSplashScreen::~mSplashScreen()
{
    delete ui;
}

void mSplashScreen::setBarRange(int min, int max)
{
    bar->setRange(min,max);
}

void mSplashScreen::setBarValue(int value)
{
    bar->setValue(value);
}

void mSplashScreen::init()
{
    initSize();
    initBar();
}

void mSplashScreen::initSize()
{
    if(!bannerMap.isNull())
    {
        QRect rect=this->geometry();

        rect.setWidth(bannerMap.width());
        rect.setHeight(bannerMap.height());

        this->setGeometry(rect);
    }
}

void mSplashScreen::initBar()
{
    bar=new QProgressBar(this);
    bar->setGeometry(0,bannerMap.height()-10,bannerMap.width(),10);
    bar->setValue(0);
}
