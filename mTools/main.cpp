#include "widget.h"

#include <QApplication>
#include <QDebug>

#include "msplashscreen.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QPixmap map(":/images/screen.jpg");

    mSplashScreen splash(map);
    splash.show();

    a.processEvents();

    splash.setBarRange(0,10000);
    splash.setBarValue(0);

    for (int i = 0; i < 10000; i++)
    {
        splash.setBarValue(i);
    }

    Widget w;
    w.show();

    splash.finish(&w);

    return a.exec();
}
