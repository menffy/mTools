#ifndef MENUITEM_H
#define MENUITEM_H

#include <QWidget>

struct menuItem
{
    int id;
    QString widgetName;
    int parentId;
    QString title;
    int icon;
    int level;
    bool isSub;
};

#endif // MENUITEM_H
