#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QLabel>

#include "menuItem.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    Ui::Widget *ui;

private slots:
    void openNewWidget(menuItem menuInfo);
    void closeTab(menuItem menuInfo);
    void selectTabClick(menuItem menuInfo);
    void fullScreen(bool flag);

private:
   void initFont();
   void initMenu();
   void initDesktop();
   void init();
   void setSkin(QString skinName);
   void setTab(menuItem menuInfo);
   void removeNewWidget(menuItem menuInfo);

private:
    QFont mFont;
    QSpacerItem *tab_horizontalSpacer_right;
};
#endif // WIDGET_H
