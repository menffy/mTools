#ifndef MTABMENU_H
#define MTABMENU_H

#include <QWidget>
#include <QFont>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QMouseEvent>

#include "menuItem.h"

class mTabMenu : public QWidget
{
    Q_OBJECT
public:
    explicit mTabMenu(menuItem menuInfo,QFont font,QWidget *parent = nullptr);

signals:
    void closeTabClick(menuItem menuInfo);
    void selectTabClick(menuItem menuInfo);

private slots:
    void closeTabBtn();
    void titleTabBtn();

private:
    init();

private:
    QGridLayout *gridLayout;
    QPushButton *titleBtn;
    QPushButton *closeBtn;
    QSpacerItem *horizontalSpacer_left;
    QSpacerItem *horizontalSpacer_right;

private:
    menuItem mMenu;
    QFont mFont;

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

};

#endif // MTABMENU_H
