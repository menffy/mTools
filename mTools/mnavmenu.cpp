#include "mnavmenu.h"
#include <QFontDatabase>
#include <QFont>

mNavMenu::mNavMenu(const QFont font,QWidget *parent) : QWidget(parent)
{
    mFont=font;
    thisParent=parent;

    init();
}

void mNavMenu::init()
{
    verticalLayout= new QVBoxLayout(thisParent);
    verticalLayout->setMargin(0);
    verticalLayout->setSpacing(0);

    menuLabel=new QLabel(this);
    menuLabel->setObjectName("menuFunction");
    menuLabel->setText(QChar(0xf0c9));
    menuLabel->setAlignment(Qt::AlignCenter);
    menuLabel->setFont(mFont);

    verticalLayout->addWidget(menuLabel);
}

void mNavMenu::setMenus(QList<menuItem> menuitems)
{
    foreach (menuItem item, menuitems) {

        mNavItem *menu=new mNavItem(mFont,item);
        connect(menu,SIGNAL(showSubMenus(int,bool)),this,SLOT(showSubMenus(int,bool)));
        connect(menu,SIGNAL(openWidget(menuItem)),this,SLOT(openWidget(menuItem)));

        if(item.level!=0)
        {
            menu->hide();
        }

        verticalLayout->addWidget(menu);
    }

    verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
    verticalLayout->addItem(verticalSpacer);
}

void mNavMenu::showSubMenus(int parentId, bool isShow)
{
    QList<mNavItem *> mNavItems=thisParent->findChildren<mNavItem *>();

    for(int i=0;i<mNavItems.count();i++)
    {
        mNavItem *menu=mNavItems.at(i);

        if(menu->getParentId()==parentId)
        {
            if(isShow)
            {
                menu->show();
            }
            else
            {
                menu->hide();
            }
        }
    }
}

void mNavMenu::openWidget(menuItem menuInfo)
{
    emit openNewWidget(menuInfo);
}
