#ifndef MNAVMENU_H
#define MNAVMENU_H

#include <QWidget>
#include <QVBoxLayout>
#include <QFrame>

#include "mnavitem.h"
#include "menuItem.h"

class mNavMenu : public QWidget
{
    Q_OBJECT
public:
    explicit mNavMenu(const QFont font,QWidget *parent = nullptr);

private:
    void init();

public:
    void setMenus(QList<menuItem> menuitems);

signals:
    void openNewWidget(menuItem menuInfo);

private slots:
    void showSubMenus(int parentId,bool isShow);
    void openWidget(menuItem menuInfo);

private:
    QLabel *menuLabel;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QWidget *thisParent;

    QFont mFont;
};

#endif // MNAVMENU_H
