#include "desktop.h"
#include "ui_desktop.h"

DeskTop::DeskTop(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DeskTop)
{
    ui->setupUi(this);
}

DeskTop::~DeskTop()
{
    delete ui;
}
