#ifndef MNAVITEM_H
#define MNAVITEM_H

#include <QWidget>
#include <QLabel>
#include <QFontDatabase>
#include <QChar>
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QMessageBox>

#include <menuItem.h>

class mNavItem : public QWidget
{
    Q_OBJECT
public:
    explicit mNavItem(const QFont &font,
                      const menuItem &menu,
                      QWidget *parent = nullptr);
signals:
    void showSubMenus(int parentId,bool isShow);
    void openWidget(menuItem menuInfo);

private:

    QFont mFont;
    menuItem mMenu;

private:
    QLabel *textLabel;
    QLabel *subLabel;
    QHBoxLayout *horizontalLayout;

private:
    void init();
public:
    int getParentId();

protected:
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
};

#endif // MNAVITEM_H
