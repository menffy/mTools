#ifndef MSPLASHSCREEN_H
#define MSPLASHSCREEN_H

#include <QWidget>
#include <QSplashScreen>
#include <QPixmap>
#include <QProgressBar>

namespace Ui {
class mSplashScreen;
}

class mSplashScreen : public QSplashScreen
{
    Q_OBJECT

public:
    explicit mSplashScreen(const QPixmap &pixmap = QPixmap(),QWidget *parent = nullptr);
    ~mSplashScreen();

    void setBarRange(int min,int max);
    void setBarValue(int value);

private:
    Ui::mSplashScreen *ui;
    QPixmap bannerMap;
    QProgressBar *bar;

private:
    void init();
    void initSize();
    void initBar();

};

#endif // MSPLASHSCREEN_H
