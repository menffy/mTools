#include "mnavitem.h"

#include <QDebug>
#include <QStyleOption>
#include <QPainter>

mNavItem::mNavItem(const QFont &font,
                   const menuItem &menu,
                   QWidget *parent): QWidget(parent)
{
    this->setMouseTracking(true);

    mFont=font;
    mMenu=menu;

    init();
}

void mNavItem::init()
{
    QString space="";

    for (int i = 0; i < ((mMenu.level+1)*4); ++i) {
        space+=" ";
    }

    QString str=QString("%1%2%3%4").arg(space).arg(QChar(mMenu.icon)).arg(tr("  ")).arg(mMenu.title);

    horizontalLayout = new QHBoxLayout(this);
    horizontalLayout ->setMargin(0);
    horizontalLayout ->setSpacing(0);

    textLabel=new QLabel(this);
    textLabel->setFont(mFont);
    textLabel->setText(str);

    //textLabel->setStyleSheet("color:white;");
    //textLabel->setMargin(0);
    //textLabel->setMinimumSize(0,36);

    textLabel->setAttribute(Qt::WA_TransparentForMouseEvents);

    horizontalLayout ->addWidget(textLabel);

    subLabel=new QLabel(this);
    subLabel->setFont(mFont);
    subLabel->setMaximumSize(10,this->height());

    //subLabel->setStyleSheet("color:white;");
    //subLabel->setMargin(0);
    //subLabel->setMinimumSize(0,36);

    subLabel->setAttribute(Qt::WA_TransparentForMouseEvents);

    if(mMenu.isSub)
    {
        subLabel->setText(QChar(0xf104));
    }else
    {
        subLabel->setText("");
    }

    horizontalLayout->addWidget(subLabel);
}

int mNavItem::getParentId()
{
    return mMenu.parentId;
}

void mNavItem::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
        if(mMenu.isSub)
        {
            if(subLabel->text()==QChar(0xf104))
            {
                subLabel->setText(QChar(0xf107));
                emit showSubMenus(mMenu.id,true);
            }
            else
            {
                subLabel->setText(QChar(0xf104));
                emit showSubMenus(mMenu.id,false);
            }
        }
        else
        {
            emit openWidget(mMenu);
        }
    }
}

void mNavItem::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.init(this);

    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
